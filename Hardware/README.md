The .f3d files in this folder are created with Fusion 360. Feel free to modify them, for example if you use a different (size) battery.

There's also a laser cut frame available now, also designed in Fusion 360. For exporting, the sketches containing the _DXF suffix contain all the laser cuttable files. The design is fully parametrized, so you can change the sheet thickness. 

Some instructions for assembling the frame (assuming the PCB is fully ready):
1. First attach the motors to their mounting plates. They are fitted closely together, so they can only be mounted when the frame is not fully assembled. 
2. Put the front plate, face down, on a table. Now, assemble the inner frame (see picture). Note that both the motor mounting plates, and the plates surrounding the battery case, have directionality; the battery case plates have cut-outs for the nuts of the PCB mounting holes.
3. Mount the PCB, and add the battery holder (if not already soldered)
4. Add the wiring from the stepper motors to the PCB. Best is to first try whether the cables are in the correct polarity, before soldering; glueing. Trial and error :)
5. Test the robot
6. When everything works, assemble the back panel, and the upper side plates.
7. Use a few dots of superglue to fix everything in place. 

![Inner frame](/Hardware/laserCutFrame.jpg)