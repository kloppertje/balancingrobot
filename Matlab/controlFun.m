function Cout = controlFun(state, controlPar)

phiRef = controlPar.phiRef;

phi = state(:,2);
phid = state(:,4);

e = phiRef - phi;
ed = -phid;

Cout = e*controlPar.Kp + ed*controlPar.Kd;

% Limit controller output
Cout(Cout>controlPar.max) = controlPar.max;
Cout(Cout<controlPar.min) = controlPar.min;
