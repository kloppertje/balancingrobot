function stateDerivative = odeFun(state, parVal, controlPar)

Cout = controlFun(state, controlPar);
stateDerivative = stateDerivativeFun(state, parVal, Cout);