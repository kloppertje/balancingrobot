% Read data from robot, and simulate sensor algorithm
%
% Wouter Klop
% 23-02-2021
% wouter@elexperiment.nl


set(0,'DefaultAxesXGrid','on')

dataPath = 'data/motion/';

fieldNames = {'accAngle','filterAngle',...
    'setpoint1','input1','output1',...
    'setpoint2','input2','output2',...
    'ay','az','gx'};

%% Find all .csv files
files = dir([dataPath, '*.csv']);
fileNames = {files.name}.';

%% Read .csv file
file = fileNames{1};
dRaw = csvread([dataPath, file], 1, 0);

t = dRaw(:,1) - dRaw(1,1);
d = cell2struct(num2cell(dRaw(:,2:end),1), fieldNames,2);

dT = mean(diff(t));
Fs = 1/dT;

figure(1); clf;
subplot(211)
plot(t,d.ay)
hold on
plot(t, d.az)
hold off

subplot(212)
plot(t,d.gx)

